﻿using GA;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Text;

namespace TestProject2
{
    
    //test
    /// <summary>
    ///This is a test class for OptymalizatorGenetycznyTest and is intended
    ///to contain all OptymalizatorGenetycznyTest Unit Tests
    ///</summary>
    ///
    [DeploymentItem("test.arff")]
    [TestClass()]
    public class OptymalizatorGenetycznyTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion

        Form1 form; // TODO: Initialize to an appropriate value
        string nazwaPliku, nazwaPlikuTestowego;
        int typSelekcji, typKrzyżowania;
        
        [TestInitialize()]
        public void MyTestInitialize()
        {
             form = null; // TODO: Initialize to an appropriate value
             nazwaPliku = "test.arff";
             nazwaPlikuTestowego = "test.arff";
             typSelekcji = 0;
             typKrzyżowania = 0;  
        }
        OptymalizatorGenetyczny getOG()
        {
            Parametry p = new Parametry
            {
                CM = 0.5,
                CR = 0.5,
                maxT = 10
            };
            return new OptymalizatorGenetyczny(form, nazwaPliku, nazwaPlikuTestowego, typSelekcji, typKrzyżowania,p);
        }
        /// <summary>
        ///A test for podob
        ///</summary>
        [TestMethod()]
        [DeploymentItem("GA.exe")]
        public void podobTest()
        {
            //PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            OptymalizatorGenetyczny_Accessor target = new OptymalizatorGenetyczny_Accessor();
            Wzorzec_Accessor w1 = new Wzorzec_Accessor(); // TODO: Initialize to an appropriate value
            w1.funkcjaOceny = 1;
            Wzorzec_Accessor w2 = new Wzorzec_Accessor(); // TODO: Initialize to an appropriate value
            w2.funkcjaOceny = 1;
            double expected = 0; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.podob(w1, w2);
            Assert.AreEqual(expected, actual, 0.00001);

            w1 = new Wzorzec_Accessor(); // TODO: Initialize to an appropriate value
            w1.funkcjaOceny = 0.4;
            w2 = new Wzorzec_Accessor(); // TODO: Initialize to an appropriate value
            w2.funkcjaOceny = 0.7;
            expected = 0.3; // TODO: Initialize to an appropriate value
            actual = target.podob(w1, w2);
            Assert.AreEqual(expected, actual, 0.00001);

            w1 = new Wzorzec_Accessor(); // TODO: Initialize to an appropriate value
            w1.funkcjaOceny = 0.7;
            w2 = new Wzorzec_Accessor(); // TODO: Initialize to an appropriate value
            w2.funkcjaOceny = 0.4;
            expected = 0.3; // TODO: Initialize to an appropriate value
            actual = target.podob(w1, w2);
            Assert.AreEqual(expected, actual, 0.00001);
      
            //Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for pasujeDoWzorca
        ///</summary>
        [DeploymentItem("GA.exe")]
        public void pasujeDoWzorcaTest()
        {
            PrivateObject param0 = null; // TODO: Initialize to an appropriate value
            OptymalizatorGenetyczny_Accessor target = new OptymalizatorGenetyczny_Accessor(param0); // TODO: Initialize to an appropriate value
            Wzorzec_Accessor wzorzec = null; // TODO: Initialize to an appropriate value
            Wzorzec_Accessor obiekt = null; // TODO: Initialize to an appropriate value
            bool expected = false; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.pasujeDoWzorca(wzorzec, obiekt);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for funkcjaOceny
        ///</summary>
        public void funkcjaOcenyTest()
        {
            OptymalizatorGenetyczny target = new OptymalizatorGenetyczny();
            Wzorzec w = null; // TODO: Initialize to an appropriate value
            double expected = 0F; // TODO: Initialize to an appropriate value
            double actual;
            actual = target.funkcjaOceny(w);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for Krzyzowanie
        ///</summary>
        [TestMethod()]
        public void KrzyzowanieTest()
        {
            var tablicaAtrybutów = new string[] { "a1", "a2", "a3", "a4" };
            OptymalizatorGenetyczny target = new OptymalizatorGenetyczny(tablicaAtrybutów); // TODO: Initialize to an appropriate value
            
            Wzorzec p1 = new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 3, 9 }); // TODO: Initialize to an appropriate value
            Wzorzec p2 = new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 5, 6 }); // TODO: Initialize to an appropriate value
            Wzorzec expected1 = new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 3, 9 }); // TODO: Initialize to an appropriate value
            Wzorzec expected2 = new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 5, 6 }); // TODO: Initialize to an appropriate value
            Wzorzec[] actual;
            actual = target.Krzyzowanie(p1, p2);
            Assert.AreEqual(expected1, actual[0]);
            Assert.AreEqual(expected2, actual[1]);
            
        }

        /// <summary>
        ///A test for KrzyzowanieDwupunktowe
        ///</summary>
        [TestMethod()]
        public void KrzyzowanieDwupunktoweTest()
        {
            var tablicaAtrybutów = new string[] { "a1", "a2", "a3", "a4" };
            OptymalizatorGenetyczny target = new OptymalizatorGenetyczny(tablicaAtrybutów); // TODO: Initialize to an appropriate value

            Wzorzec p1 = new Wzorzec(tablicaAtrybutów, new object[] { 1, 2, 3, 4 }); // TODO: Initialize to an appropriate value
            Wzorzec p2 = new Wzorzec(tablicaAtrybutów, new object[] { 5, 6, 7, 8 }); // TODO: Initialize to an appropriate value
            Wzorzec expected1 = new Wzorzec(tablicaAtrybutów, new object[] { 1, 2, 7, 4 }); // TODO: Initialize to an appropriate value
            Wzorzec expected2 = new Wzorzec(tablicaAtrybutów, new object[] { 5, 6, 3, 8 }); // TODO: Initialize to an appropriate value
            Wzorzec[] actual;
            actual = target.KrzyzowanieDwupunktowe(p1, p2);
            Assert.AreEqual(expected1, actual[0]);
            Assert.AreEqual(expected2, actual[1]);
        }

        /// <summary>
        ///A test for KrzyzowanieJednopunktowe
        ///</summary>
        [TestMethod()]
        public void KrzyzowanieJednopunktoweTest()
        {
            var tablicaAtrybutów = new string[] { "a1", "a2", "a3", "a4" };
            OptymalizatorGenetyczny target = new OptymalizatorGenetyczny(tablicaAtrybutów);

            Wzorzec p1 = new Wzorzec(tablicaAtrybutów, new object[] { 1, 2, 3, 4 });
            Wzorzec p2 = new Wzorzec(tablicaAtrybutów, new object[] { 5, 6, 7, 8 });
            Wzorzec expected1 = new Wzorzec(tablicaAtrybutów, new object[] { 5, 6, 3, 4 });
            Wzorzec expected2 = new Wzorzec(tablicaAtrybutów, new object[] { 1, 2, 7, 8 });
            Wzorzec[] actual;
            actual = target.KrzyzowanieJednopunktowe(p1, p2);
            Assert.AreEqual(expected1, actual[0]);
            Assert.AreEqual(expected2, actual[1]);
        }

        /// <summary>
        ///A test for run
        ///</summary>
        [TestMethod()]
        public void runTest()
        {
            OptymalizatorGenetyczny target = getOG(); // TODO: Initialize to an appropriate value
            StringBuilder expected = new StringBuilder(
                "q=p, w=b, e=s, -> r=BDCRLFw=x, -> r=n (33%)CRLFq=p, e=s, -> r=n (100%)CRLFq=e, w=x, e=s, -> r=y (100%)CRLFw=x, e=y, -> r=w (100%)CRLFq=p, -> r=n (50%)CRLFq=p, w=x, -> r=n (50%)CRLFq=e, w=x, -> r=y (100%)CRLFq=e, w=x, e=y, -> r=BDCRLFq=p, w=x, e=s, -> r=n (100%)CRLF"
                ); // TODO: Initialize to an appropriate value
            StringBuilder actual;
            actual = target.run();
            var passed=true;
            for (int i = 0; i < expected.Length; i++)
                if (actual[i] != expected[i])
                    passed=false;

            Assert.IsTrue(passed);
            
        }
    }
}
