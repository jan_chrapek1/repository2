﻿using GA;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject2
{
    
    
    /// <summary>
    ///This is a test class for SelekcjaRankingowaTest and is intended
    ///to contain all SelekcjaRankingowaTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SelekcjaRankingowaTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Selekcja
        ///</summary>
        [TestMethod()]
        public void SelekcjaTest()
        {
            var tablicaAtrybutów = new string[] { "a1", "a2", "a3", "a4" };

            SelekcjaRankingowa target = new SelekcjaRankingowa(); // TODO: Initialize to an appropriate value
            Wzorzec[] populacja = new Wzorzec[]
                {
                    new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 3, 9 }),
                    new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 4, 5 }),
                    new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 5, 8 }),
                    new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 7, 8 }),
                    new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 2, 7 })
                }; // TODO: Initialize to an appropriate value
            for (int i = 0; i < populacja.Length; i++)
                populacja[i].funkcjaOceny = 0.1 * i;

            int[] expected = { 4, 4 }; // TODO: Initialize to an appropriate value
            int[] actual = null;
            actual = target.Selekcja(populacja);
            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
