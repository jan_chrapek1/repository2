﻿using GA;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace TestProject2
{
    
    
    /// <summary>
    ///This is a test class for WzorzecTest and is intended
    ///to contain all WzorzecTest Unit Tests
    ///</summary>
    [TestClass()]
    public class WzorzecTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion



        /// <summary>
        ///A test for Equals
        ///</summary>
        [TestMethod()]
        public void EqualsTest()
        {
            var tablicaAtrybutów1 = new string[] { "a1", "a2", "a3", "a4" };
            Wzorzec p0 = new Wzorzec(tablicaAtrybutów1, new object[] { 1, 2, 3, 4 });
            Wzorzec p1 = new Wzorzec(tablicaAtrybutów1, new object[] { 1, 2, null, 4 }); // TODO: Initialize to an appropriate value
            Wzorzec p2 = new Wzorzec(tablicaAtrybutów1, new object[] { 1, 2, 3, null }); // TODO: Initialize to an appropriate value
            Wzorzec p3 = new Wzorzec(tablicaAtrybutów1, new object[] { 1, 2, 3, 5}); // TODO: Initialize to an appropriate value

            bool b1 = p0.Equals(p1);
            bool b2 = p0.Equals(p2);
            bool b3 = p0.Equals(p3);
            
            Assert.IsFalse(b1||b2||b3);
        }
    }
}
