﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA
{
    class SelekcjaProporcjonalna:ISelection
    {
        
        public int[] Selekcja(Wzorzec[] populacja)
        {
            var p1 = Wybierz(populacja);
            var p2 = Wybierz(populacja);
            int i = 0;
            while (p1 == p2 && i < 2)
            {
                p2 = Wybierz(populacja);
                i++;
            }
            
            return new int[] { p1, p2 };
        }
        int Wybierz(Wzorzec[] populacja)
        {            
            double totalScore = 0;
            double runningScore = 0;
            foreach (Wzorzec g in populacja)
            {
                totalScore += g.funkcjaOceny;
            }

            double rnd = (double) (new Random(0).NextDouble() * totalScore);

            for(int i=0; i<populacja.Length; i++)
            {
                Wzorzec g = populacja[i];
                var f = g.funkcjaOceny;
                if (rnd>=runningScore &&
                    rnd<=runningScore+f)
                {
                    return i;
                }
                runningScore+=f;
            }

            return -1;
        }
        
    }
}
