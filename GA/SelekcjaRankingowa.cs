﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA
{
    class SelekcjaRankingowa:ISelection
    {
        public int[] Selekcja(Wzorzec[] populacja)
        {
            var p1 = Wybierz(populacja);
            var p2 = Wybierz(populacja);
            int i = 0;
            while (p1 == p2 && i < 2)
            {
                p2 = Wybierz(populacja);
                i++;
            }

            return new int[] { p1, p2 };
        }
        int Wybierz(Wzorzec[] populacja)
        {
            double totalScore = 0;
            double runningScore = 0;
            int[] ranking = new int[populacja.Length];

            for (int i = 0; i < populacja.Length; i++)
            {
                ranking[i] = (from w in populacja
                              where w.funkcjaOceny <= populacja[i].funkcjaOceny
                              select w).Count();
                totalScore += ranking[i];
            }

            double rnd = (double)(new Random(0).NextDouble() * totalScore);

            for (int i = 0; i < populacja.Length; i++)
            {

                var f = ranking[i];
                if (rnd >= runningScore &&
                    rnd <= runningScore + f)
                {
                    return i;
                }
                runningScore += f;
            }

            return -1;
        }
    }
}
