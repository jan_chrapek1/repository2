﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace GA
{
    class Wzorzec:Dictionary<string,object>
    {
        double fo;
        public Wzorzec()
        {
            funkcjaOceny = -1;
        }
        public double funkcjaOceny {
            get
            {
               //if (fo < 0) throw new Exception();
                return fo;
            }
            set
            {
                fo = value;
            }
        }

        //do celów testowych
        public Wzorzec(string[] tablicaAtrybutów, object[] WartościAtrybutów)
        {
            funkcjaOceny = -1;
            for(int i=0; i<tablicaAtrybutów.Length; i++)
                Add(tablicaAtrybutów[i],WartościAtrybutów[i]);
        }

        //del
        //string[] NazwyAtrybutów;
        //object[] WartościAtrybutów;
        //public Wzorzec(string[] s, object[] o)
        //{
        //    NazwyAtrybutów = s;
        //    WartościAtrybutów = o;
        //}

        internal Wzorzec Clone()
        {
            Wzorzec w = new Wzorzec();
            foreach(var k in Keys)
                w.Add(k, this[k]);
            w.funkcjaOceny = funkcjaOceny;
            return w;
            
        }
        public override bool Equals(object obj)
        {
            Wzorzec w = obj as Wzorzec;
            if (obj == null) return false;
            foreach (var k in Keys)
            {
                if (!(this[k] == null) && w[k] == null) return false;
                if (!(w[k] == null) && this[k] == null) return false;
                if (w[k] == null && this[k] == null) continue;
                if (!this[k].Equals(w[k])) return false;
            }
            return true;

        }

        internal SelekcjaRankingowa SelekcjaRankingowa
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        internal SelekcjaTurniejowa SelekcjaTurniejowa
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        internal SelekcjaProporcjonalna SelekcjaProporcjonalna
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        //public object this[string key]
        //{
        //    get {
        //        return base[key];
        //    }
        //    set {
        //        base[key] = value;
        //    }
        //}
    }
}
