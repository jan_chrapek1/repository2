﻿//3 testy jednostkowe

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms.DataVisualization.Charting;

namespace GA
{
    struct Parametry
    {
        public double CR, CM;
        public int maxT;
    }
    class OptymalizatorGenetyczny
    {
        Parametry parametry;
        string[] tablicaAtrybutów, atrybutyWarunkowe;
        string atrybutDecyzyjny;
        List<Wzorzec> tablicaDecyzyjna;
        Random rand;
        Form1 form;
        int typSelekcji, typKrzyżowania;
        private string nazwaPlikuTestowego;
        public OptymalizatorGenetyczny()
        {
            rand = new Random(0);
        }
        public OptymalizatorGenetyczny(string[] atrybutyWarunkowe)
        {
            rand = new Random(0);
            this.atrybutyWarunkowe = atrybutyWarunkowe;
        }
        public OptymalizatorGenetyczny(Form1 form, string nazwaPliku, string nazwaPlikuTestowego, int typSelekcji, int typKrzyżowania, Parametry parametry)
        {
            rand = new Random(0);
            this.form = form;
            this.typSelekcji = typSelekcji;
            this.typKrzyżowania = typKrzyżowania;
            this.parametry = parametry;
            wczytajDane(nazwaPliku);
            this.nazwaPlikuTestowego = nazwaPlikuTestowego;
            run();
        }

        internal Wzorzec Wzorzec
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        private void wczytajDane(string nazwaPliku)
        {
            StreamReader sr = File.OpenText(nazwaPliku);
            string f = sr.ReadToEnd();
            sr.Close();

            var linie = usunKomentarze(ref f);

            string pattern = @"@attribute ('?)(.*)(\1) (.*)";
            string text = f;

            MatchCollection matches;

            Regex defaultRegex = new Regex(pattern);
            // Get matches of pattern in text
            matches = defaultRegex.Matches(text);

            tablicaAtrybutów = new string[matches.Count];

            // Iterate matches
            for (int ctr = 0; ctr < matches.Count; ctr++)
                tablicaAtrybutów[ctr] = matches[ctr].Groups[2].Value;

            //---------
            //tablicaAtrybutów = wczytajAtrybuty();
            atrybutyWarunkowe = new string[tablicaAtrybutów.Length - 1];
            atrybutDecyzyjny = tablicaAtrybutów.Last();
            Array.Copy(tablicaAtrybutów, 0, atrybutyWarunkowe, 0, atrybutyWarunkowe.Length);
            //typyAtrybutów = new Type[] { typeof(int), typeof(int), typeof(int), typeof(int) };
            
            //tablicaDecyzyjna = wczytajTabliceDecyzyjną();

            tablicaDecyzyjna = new List<Wzorzec>();
            var data = false;
            for (int i = 0; i < linie.Count; i++)
            {
                if (linie[i].StartsWith("@data"))
                {
                    data = true;
                    continue;
                }
                if (data)
                {
                    if (linie[i] == "") continue;
                    Wzorzec w = new Wzorzec();
                    var atrybuty = linie[i].Split(',');
                    for(int j=0; j<atrybuty.Length; j++)
                    {
                        Regex r = new Regex(@"('?)(.*)(\1)");
                        var wartość = r.Matches(atrybuty[j])[0].Groups[2].Value;
                        w[tablicaAtrybutów[j]] = wartość;                        
                    }
                    
                    tablicaDecyzyjna.Add(w);
                }
            }
        }

        private List<string> usunKomentarze(ref String input)
        {    
            string result = "";
            var linie = new List<string>();
            foreach (string line in Regex.Split(input, @"\r|\n|\r\n"))
                if(!line.StartsWith("%"))
                {
                    result += line + "\n";
                    linie.Add(line);
                }
            input = result;
            return linie;
        }
        public StringBuilder run()
        {
            ISelection sel= new SelekcjaTurniejowa();
            switch (typSelekcji)
            {
                case 1:
                    sel = new SelekcjaProporcjonalna();
                    break;
                case 2:
                    sel = new SelekcjaRankingowa();
                    break;
            }
            int t = 0;                        
            var tmp_populacja = StworzPopulacje(parametry.maxT + 1, atrybutyWarunkowe);
            /*foreach (Wzorzec w in populacjeBazowe[0])
                form.textBox1.AppendText("\n"+w.ToString());*/
            DataPointCollection ptsAvg = null, ptsMax = null;
            if (form != null)
            {
                ptsAvg = form.chart1.Series[0].Points;
                ptsMax = form.chart1.Series[1].Points;
                ptsAvg.Clear();
                ptsMax.Clear();
            }
            while (t < parametry.maxT)
            {
                Ocena(tmp_populacja);
                for(int i=0; i<tmp_populacja.Length; i++) //while (sa_osobnicy)
                {
                    int[] p = sel.Selekcja(tmp_populacja);
                    Wzorzec[] P = new Wzorzec[]
                    {
                        tmp_populacja[p[0]],
                        tmp_populacja[p[1]],
                    };
                    Wzorzec[] C =  { P[0], P[1] };
                    switch(typKrzyżowania)
                    {
                        case 0:
                            C = Krzyzowanie(P[0], P[1]);
                            break;
                        case 1:
                            C = KrzyzowanieJednopunktowe(P[0], P[1]);
                            break;
                        case 2:
                            C = KrzyzowanieDwupunktowe(P[0], P[1]);
                            break;
                    }
                    Mutowanie(C[0]);
                    Mutowanie(C[1]);

                    Ocena(C);

                    if (rand.NextDouble() < 0.4)//dla 40% przypadków
                    {
                        if (P[0].funkcjaOceny > C[0].funkcjaOceny)
                            C[0] = P[0];
                        if (P[1].funkcjaOceny > C[1].funkcjaOceny)
                            C[1] = P[1];
                    }
                    //if(niszowanie)
                    C = Niszowanie(P[0], P[1], C[0], C[1]);
                                        
                    tmp_populacja[p[0]] = C[0];
                    tmp_populacja[p[1]] = C[1];

                }
                var o = (from w in tmp_populacja select w.funkcjaOceny).ToArray();
                if (form != null)
                {
                    ptsAvg.AddY(o.Average());
                    ptsMax.AddY(o.Max());
                    form.Update();
                }
                t++;
            }
            if(form!=null) form.textBox1.ResetText();

            
            List<string> reguły = new List<string>();
            List<object> wybraneDecyzje = new List<object>();
            foreach (Wzorzec w in tmp_populacja)
            {                
                var decyzje =
                    from o in tablicaDecyzyjna
                        where pasujeDoWzorca(w, o)
                        select o[atrybutDecyzyjny];
                object mode = null;
                if (decyzje.Count() > 0)
                {
                    var groups = decyzje.GroupBy(v => v);
                    int maxCount = groups.Max(g => g.Count());
                    mode = groups.First(g => g.Count() == maxCount).Key;
                }
                    
                wybraneDecyzje.Add(mode);

                string reguła = "";
                for (int i = 0; i < atrybutyWarunkowe.Length; i++)
                {
                    var atr = atrybutyWarunkowe[i];
                    if(w[atr]!=null) reguła += String.Format("{0}={1}, ",atr, w[atr]);                                                
                }
                reguła += String.Format("-> {0}={1}", atrybutDecyzyjny, mode==null?"BD":mode);
                reguły.Add(reguła);
                
            }

            //StringBuilder result = new StringBuilder();
            //var tablicaReguł = reguły.Distinct().ToArray();
            //foreach (var s in tablicaReguł)
            //{
            //    result.Append(s + "\n");
            //}

            //testowanie reguł
            List<double> listaDokladnosci = new List<double>();
            wczytajDane(nazwaPlikuTestowego);
            for (int i = 0; i < tmp_populacja.Length; i++)
            {
                Wzorzec w = tmp_populacja[i];
                var decyzje =
                    from o in tablicaDecyzyjna
                    where pasujeDoWzorca(w, o)
                    select o[atrybutDecyzyjny];
                var poprawne = from d in decyzje
                               where d.Equals(wybraneDecyzje[i])
                               select d;
                if(decyzje.Count()>0)
                {
                    double dokladność = ((double)poprawne.Count()) / ((double)decyzje.Count());
                    listaDokladnosci.Add(dokladność);
                    reguły[i] += String.Format(" ({0}%)", (int)(dokladność * 100));
                }
                
            }

            StringBuilder result1 = new StringBuilder();
            StringBuilder result2 = new StringBuilder();
            var tablicaReguł2 = reguły.Distinct().ToArray();
            foreach (var s in tablicaReguł2)
            {
                result1.Append(s + "CRLF");
                result2.Append(s + Environment.NewLine);
            }
            result2.Append(String.Format("Średnia dokładność: {0}%", (int)(listaDokladnosci.Average() * 100)));
            if (form != null) form.textBox1.Text=result2.ToString();

            return result1;
        }

        private Wzorzec[] Niszowanie(Wzorzec p1, Wzorzec p2, Wzorzec c1, Wzorzec c2)
        {
            Wzorzec[] result = new Wzorzec[2];
            if (podob(p1, c1) + podob(p2, c2) > podob(p1, c2) + podob(p2, c1))
            {
                //p1 jest podobne do c2, a p2 - do c1
                if (c2.funkcjaOceny > p1.funkcjaOceny)
                    result[0] = c2;
                else
                    result[0] = p1;

                if (p2.funkcjaOceny > c1.funkcjaOceny)
                    result[1] = p2;
                else
                    result[1] = c1;
            }
            else
            {
                //p1 jest podobne do c1, a p2 - do c2
                if (c1.funkcjaOceny > p1.funkcjaOceny)
                    result[0] = c1;
                else
                    result[0] = p1;

                if (p2.funkcjaOceny > c2.funkcjaOceny)
                    result[1] = p2;
                else
                    result[1] = c2;
            }
            return result;

        }
        private double podob(Wzorzec w1, Wzorzec w2)
        {
            return Math.Abs(w1.funkcjaOceny - w2.funkcjaOceny);
        }

        //private List<Wzorzec> wczytajTabliceDecyzyjną()
        //{
        //    List<Wzorzec> result = new List<Wzorzec>(4);
            
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 3, 9 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 4, 5 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 5, 8 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 7, 8 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 2, 7 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 1, "2", 4, 4 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 4, "5", 6, 0 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 7, "8", 9, 6 }));
        //    result.Add( new Wzorzec(tablicaAtrybutów, new object[] { 10, "11", 12, 2 }));
            
        //    return result;
        //}

        //private string[] wczytajAtrybuty()
        //{
        //    return new string[]{"a1","a2","a3","a4"};
        //}
        public Wzorzec[] KrzyzowanieJednopunktowe(Wzorzec p1, Wzorzec p2)
        {
            var c1 = p1.Clone();
            var c2 = p2.Clone();

            int podzial = rand.Next(p1.Count);
            for (int i = 0; i < podzial; i++)
            {
                c1[atrybutyWarunkowe[i]] = p2[atrybutyWarunkowe[i]];
                c2[atrybutyWarunkowe[i]] = p1[atrybutyWarunkowe[i]];
            }

            return new Wzorzec[] { c1, c2 };
        }
        public Wzorzec[] KrzyzowanieDwupunktowe(Wzorzec p1, Wzorzec p2)
        {
            var c1 = p1.Clone();
            var c2 = p2.Clone();

            int podzial1 = rand.Next(p1.Count);
            int podzial2 = rand.Next(p1.Count);

            for (int i = Math.Min(podzial1, podzial2); i < Math.Max(podzial1, podzial2); i++)
            {
                c1[atrybutyWarunkowe[i]] = p2[atrybutyWarunkowe[i]];
                c2[atrybutyWarunkowe[i]] = p1[atrybutyWarunkowe[i]];
            }

            return new Wzorzec[] { c1, c2 };
        }
        public Wzorzec[] Krzyzowanie(Wzorzec p1, Wzorzec p2)
        {
            var c1 = p1.Clone();
            var c2 = p2.Clone();
            foreach (string i in atrybutyWarunkowe)
            {
                if (rand.NextDouble() < parametry.CR)
                {
                    var t = c1[i];
                    c1[i] = c2[i];
                    c2[i] = t;
                }
            }
            return new Wzorzec[] { c1, c2 };

        }
        public void Mutowanie(Wzorzec mutowany)
        {
            //if(rand.NextDouble()>0.5)
            //    mutowany.funkcjaOceny += 0.1;
            //else
            //    mutowany.funkcjaOceny -= 0.1;
            
            foreach (var atrybut in atrybutyWarunkowe)
                if (rand.NextDouble() < parametry.CM)
                {
                    if (rand.NextDouble() > 0.5)
                    {
                        var obiekt = losowyObiektTablicyDecyzyjnej();
                        mutowany[atrybut] = obiekt[atrybut];
                    }
                    else
                    {
                        mutowany[atrybut] = null;
                    }
                }
        }
        private void Ocena(Wzorzec[] populacja)
        {
            for (int i = 0; i < populacja.Length; i++)
            {
                var w = populacja[i];
                w.funkcjaOceny = funkcjaOceny(w);
            }
        }
        public double funkcjaOceny(Wzorzec w)
        {
            int liczba_obiektów = tablicaDecyzyjna.Count;
            int liczba_atrybutów = w.Count;
            int norm_liczba_atrybutów = liczba_atrybutów;
            foreach (var e in w)
            {
                if (e.Value == null) norm_liczba_atrybutów--;
            }
            int nośnik_wzorca = (from t in tablicaDecyzyjna where pasujeDoWzorca(w, t) select t).Count();
            double d1 = (((double)norm_liczba_atrybutów) / liczba_atrybutów);
            double d2 = (((double)nośnik_wzorca) / liczba_obiektów);
            return d1 * d2;
        }
        private bool pasujeDoWzorca(Wzorzec wzorzec, Wzorzec obiekt)
        {
            foreach (var key in wzorzec.Keys)
            {
                
                if (wzorzec[key] != null &&
                    !(wzorzec[key].Equals(obiekt[key])))
                    return false;
            }
            return true;
        }

        private Wzorzec[] StworzPopulacje(int wielkosc, string[] tablicaAtrybutow)
        {
            Wzorzec[] populacja=new Wzorzec[wielkosc];
            for (int i = 0; i < populacja.Length; i++)
            {
                populacja[i] = new Wzorzec();
                foreach (string atrybut in tablicaAtrybutow)
                {
                    object obiekt = null;
                    if (rand.NextDouble() > 0.3)//>0.95
                    {
                        var l = losowyObiektTablicyDecyzyjnej();
                        obiekt = l[atrybut];
                    }
                    populacja[i][atrybut] = obiekt;
                }
            }
            return populacja;
        }

        private Wzorzec losowyObiektTablicyDecyzyjnej()
        {
            var index = rand.Next(tablicaDecyzyjna.Count);
            return tablicaDecyzyjna[index];
        }

    }
}
