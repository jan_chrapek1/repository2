﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GA
{
    class SelekcjaTurniejowa:ISelection
    {
        
        public SelekcjaTurniejowa()
        {
            //funkcjaOceny = f;
        }
        public int[] Selekcja(Wzorzec[] populacja)
        {
            var p1 = Wybierz(populacja);
            var p2 = Wybierz(populacja);
            int i = 0;
            while (p1 == p2 && i < 2)
            {
                p2 = Wybierz(populacja);
                i++;
            }
            /*
            if (i == 2 && rand.NextDouble() < 0.55)
                p2 = Mutowanie(p2);
            else
                p2 = Inicjacja(1,atrybutyWarunkowe)[0];
            return new Wzorzec[]{p1, p2};
             * */
            return new int[] { p1, p2 };
        }
        int Wybierz(Wzorzec[] populacja)
        {
            var p = Choice(populacja);
            for (int i = 0; i < populacja.Length / 3; i++)
            {
                var wylosowany = Choice(populacja);
                if (populacja[p].funkcjaOceny < populacja[wylosowany].funkcjaOceny)
                    p = wylosowany;
            }
            return p;
        }
        int Choice(Wzorzec[] populacja)
        {
            return new Random(0).Next(populacja.Length);
        }
    }
}
