﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        internal OptymalizatorGenetyczny OptymalizatorGenetyczny
        {
            get
            {
                throw new System.NotImplementedException();
            }
            set
            {
            }
        }

        public void Form1_Load(object sender, EventArgs e)
        {           
            comboBox1.SelectedIndex = 0;
            comboBox2.SelectedIndex = 0;
        }
        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            MessageBox.Show(((Exception)e.ExceptionObject).Message);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            Parametry p = new Parametry
            {
                CM = 0.5,
                CR = 0.5,
                maxT = 250
            };
            OptymalizatorGenetyczny og = new OptymalizatorGenetyczny(this, "mushrooms.arff", "mushrooms.dataset", comboBox1.SelectedIndex, comboBox2.SelectedIndex, p);
        }
    }
}
